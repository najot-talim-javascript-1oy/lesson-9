//  1.  Bahosi 5, 4, 3 bo’lgan o’quvchilarni ismlaridan iborat massiv qaytaruvchi getNames(baho) funksiya tuzing. (map, filter)

// const students = [
//     { name: "Quincy", percent: 96 },
//     { name: "Jason", percent: 84 },
//     { name: "Alexis", percent: 100 },
//     { name: "Sam", percent: 65 },
//     { name: "Katie", percent: 90 },
//     { name: "Anna", percent: 75 },
// ];
// function getNamesWithGrade(grade) {
//     const filStudents = students.filter((student) => {
//       if (student.percent >= 85 && student.percent <= 100 && grade === 5) {
//         return true;
//       } else if (student.percent >= 70 && student.percent < 85 && grade === 4) {
//         return true;
//       } else if (student.percent >= 60 && student.percent < 70 && grade === 3) {
//         return true;
//       }
//       return false;
//     });

//     const names = filStudents.map((student) => {
//       return student.name;
//     });

//     return names;
// }
// console.log(getNamesWithGrade(5));   


// 2.  Massivdagi bir xil so’zlar sonini hosil qiluvchi obyekt yarating. (reduce)

// const animals = ['dog', 'chicken', 'cat', 'dog', 'chicken', 'chicken', 'rabbit'];

// function countWords(arr) {
//     return arr.reduce((acc, curr) => {
//         if (curr in acc) {
//         acc[curr]++;
//         } else {
//         acc[curr] = 1;
//         }
//         return acc;
//     }, {});
// }
// console.log(countWords(animals)); 


// 3.  Massiv elementlari kvadratlaridan hosil bo’lgan massiv hosil qiling. (map)

// function Kvadrat(arr) {
//     return arr.map((el) => el * el);
// }
// const numbers = [1, 2, 3, 4, 5,];
// const kv = Kvadrat(numbers);
// console.log(kv);

// 4.  Massivdagi musbat sonlar yig’indisini hisoblang. (filter va reduce)

// function sumOfPositiveNumbers(arr) {
//     const positiveNums = arr.filter(num => num > 0);
//     const result = positiveNums.reduce((acc, num) => acc + num, 0);
//     return result;
// }  
// const arr = [1, -4, 12, 0, -3, 29, -150];
// const result = sumOfPositiveNumbers(arr);
// console.log(result); 
  
// 5. Satrdagi so’zlarning bosh harflarini oling. (split, map, join)

// function BoshHarf(text) {
//     const BshH = text.split(' ').map(name => name.charAt(0)).join('');
//     return BshH;
// }
// const Text = 'George Raymond Richard Martin';
// const BshH = BoshHarf(Text);
// console.log(BshH);

// 6.  Massivdagi eng yosh va eng qarilarni topib, ularni yoshlarini farqini toping. (sort).

// const people = [
//   {name: 'John', age: 13},
//   {name: 'Mark', age: 56},
//   {name: 'Rachel', age: 45},
//   {name: 'Nate', age: 67},
//   {name: 'Jeniffer', age: 65}
// ];
// function yosh_qari(arr) {
//     const sortArr = arr.sort((a, b) => a.age - b.age); 
//     const young = sortArr[0];
//     const old = sortArr[sortArr.length - 1];
//     return old.age - young.age;
// }
// const y_q = yosh_qari(people);
// console.log(y_q);
  

// 7.  N ta elementdan iborat massiv berilgan. Massiv elementlari orasidan juftlarini va toqlarini o'z ichiga oladigan massivlar hosil qilinsin. (filter)

// function OddEven(arr) {
//     const odd = arr.filter(num => num % 2 !== 0);
//     const even = arr.filter(num => num % 2 === 0);
//     return {odd, even};
// }
// let arry = [3, 7, 2, 8, 5, 9, 4];
// console.log(OddEven(arry));


// 8.  N ta elementdan iborat massiv berilgan. Massiv elementlari orasidan bir xil qiymatga ega bo’lganlarini o’chiruvchi dastur tuzilsin. Faqat birinchi uchragani qoldirilsin. (reduce)

// function removenumber(arr) {
//     return arr.reduce((nbitta, j_qiymat, index) => {
//       if (index === 0) {
//         nbitta.push(j_qiymat);
//       }
//        // Faqat birinchi elementni uniqueArr massiviga qo'shamiz
//       // So'nggi elementni oldingi elementlardan ajratib o'chiramiz
//       else if (!nbitta.includes(j_qiymat)) {
//         nbitta.push(j_qiymat);
//       }
//       return nbitta;
//     }, []);
// }
// const arr = [1, 2, 3, 4, 2, 3, 5, 6, 1, 7, 8];
// console.log(removenumber(arr));


// 9. Products massivini id, name, price, rating va discount bo'yicha sortlash; (sort)

// const products = [
//     { id: 1, name: "Mercedes-benz", price: 1500, rating: 15.5, discount: 1 },
//     { id: 2, name: "Bmw", price: 1000, rating: 10.7, discount: 4 },
//     { id: 3, name: "Malibu", price: 800, rating: 5.2, discount: 5 },
//     { id: 4, name: "Nexia", price: 400, rating: 3.0, discount: 10 },
//     { id: 5, name: "Spark", price: 100, rating: 1.9, discount: 25 }
// ];

// // Id
// products.sort((a, b) => a.id - b.id);
// console.log("ID Bo'yicha Tartiblash", products);

// // Name
// products.sort((a, b) => a.name - b.name);
// console.log("Name Bo'yicha Tartiblash", products);

// // Price
// products.sort((a, b) => a.price - b.price);
// console.log("Price Bo'yicha Tartiblash", products);

// // Rating
// products.sort((a, b) => a.rating - b.rating);
// console.log("Rating Bo'yicha Tartiblash", products);

// // Discout
// products.sort((a, b) => a.discount - b.discount);
// console.log("Discount Bo'yicha Tartiblash", products);


// 10. Rating bo'yicha eng kuchli product topilsin. (sort)

// function findMostPopularProduct(products) {
//     return products.sort((a, b) => b.rating - a.rating)[0];
// }

// const products = [
//     { id: 1, name: 'iPhone 13', price: 1000, rating: 10.9, discount: 10 },
//     { id: 2, name: 'Samsung', price: 700, rating: 5.8, discount: 7 },
//     { id: 3, name: 'Nokia', price: 740, rating: 3.7, discount: 6 },
//     { id: 4, name: 'Macbook', price: 950, rating: 2.6, discount: 5 },
//     { id: 5, name: 'Mi', price: 555, rating: 1.5, discount: 4 }
// ];
// const mostPopularProduct = findMostPopularProduct(products);
// console.log(mostPopularProduct);
      

// 11. Narxi eng past bo'lgan product topilsin. (sort)

// function Product(products) {
//     return products.sort((a, b) => a.price - b.price)[0];
// }
// const products = [
//     { id: 1, name: 'iPhone', price: 1000, rating: 10.9, discount: 10 },
//     { id: 2, name: 'Samsung', price: 700, rating: 5.8, discount: 7 },
//     { id: 3, name: 'Nokia', price: 740, rating: 3.7, discount: 6 },
//     { id: 4, name: 'Macbook', price: 950, rating: 2.6, discount: 5 },
//     { id: 5, name: 'Mi', price: 555, rating: 1.5, discount: 4 }
// ];
// const mProduct = Product(products);
// console.log(mProduct);


// 12. Barcha products narxlari yig'indisi topilsin. (reduce)

// function Price(products) {
//     return products.reduce((acc, curr) => acc + curr.price, 0);
// }
// const products = [
//     { id: 1, name: 'Nokia', price: 500 },
//     { id: 2, name: 'Asus', price: 1000 },
//     { id: 3, name: 'MacbookPro', price: 1500 },
// ];  
// console.log(Price(products)); 


// 13. Faqatgina products nomlaridangina iborat bo'lgan massiv qaytaring. (map)

// const products = [
//     { id: 1, name: 'MacbookPro', price: 500 },
//     { id: 2, name: 'Asus', price: 1000 },
//     { id: 3, name: 'Nokia', price: 1500 },
// ];  
// function ProNames(products) {
//     return products.map(product => product.name);
// }  
// console.log(ProNames(products));
  

// 14. Id si 5 bo'lgan elementni nomini qaytaruvchi dastur yozing. (find)

// const products = [
//     { id: 1, name: 'MacbookPro', price: 500 },
//     { id: 2, name: 'Asus', price: 1000 },
//     { id: 3, name: 'Nokia', price: 1500 },
//     { id: 4, name: 'Samsung', price: 1600 },
//     { id: 5, name: 'Hp', price: 1700 },
// ];  
// function Find(products) {
//     return products.find((el) => el.id > 4);
// }
// console.log(Find(products)); 


// 15. Id si 4 bo'lgan productni o'chiruvchi dastur yozing. (filter)

// let products = [
//     {
//       id: 6,
//       name: "Smarthpone",
//       price: 12000,
//       rating: 4.5,
//       discount: 20,
//     },
//     {
//       id: 2,
//       name: "Acer",
//       price: 10000,
//       rating: 4.3,
//       discount: 10,
//     },
//     {
//       id: 1,
//       name: "Mac book",
//       price: 17000,
//       rating: 4.7,
//       discount: 40,
//     },
//     {
//       id: 4,
//       name: "HP",
//       price: 21000,
//       rating: 4.1,
//       discount: 30,
//     },
//     {
//       id: 5,
//       name: "Dell",
//       price: 35000,
//       rating: 4.9,
//       discount: 30,
//     },
// ];
// let filProducts = products.filter((product) => product.id !== 4);
// console.log(filProducts);
  

// 16. Berilgan satrni faqatgina harflardan iborat ekanligiga tekshiring (split, every)

// function ALetters(str) {
//     let arr = str.split("");
//     let letterArr = arr.every((char) => /[a-zA-Z]/.test(char));
//     return letterArr;
// }
// console.log(ALetters("samariddin")); // true
// console.log(ALetters("macbook pro")); // false
// console.log(ALetters("SAMARIDDIN")); // true
// console.log(ALetters("12345")); // false
// console.log(ALetters("Samariddin Nurmamatov")); // false
    

// 17. Massiv truthy va falsy elementlardan tuzilgan. O’sha massivdagi truthy va falsy elementlarni alohida massivlarga ajratib object qilib qaytaruvchi getTruthyFalsy funksiya tuzing. (filter)

// function TruthyFalsy(arr) {
//     const truthy = arr.filter((elem) => Boolean(elem));
//     const falsy = arr.filter((elem) => !Boolean(elem));
//     return { truthy, falsy };
// }    
// const arr = [false, 1, 10, "", null, "samariddin", 1.13, 0];
// console.log(TruthyFalsy(arr)); 


// 18. Satr berilgan. Satrdagi so'zlar uzunligidan iborat bo'lgan massiv qaytaring. (split, map) Input: "Men Abdulaziz Programmerman" Outpu: [3, 9, 12]

// function WordLengths(txt) {
//     const words = txt.split(" ");
//     const lengths = words.map(word => word.length);
//     return lengths;
// }
// const txt = "Men Samariddin Nurmamatov";
// const wLengths = WordLengths(txt);
// console.log(wLengths);
  

// 19. Satrni bo'sh joy bor yoki yo'qligini tekshiring. (split, some) Input: "Men Abdulaziz Programmerman" Output: true

// function Jy(str) {
//     const words = str.split(" ");
//     const lengths = words.map(word => word.length);
//     return lengths.some(len => len === 0);
// }
// console.log(Jy("Men Abdulaziz Programmerman")); // false
// console.log(Jy("Men  Programmerman")); // true
  

// 20. Objectning kalit va qiymatlarining string ko'rinishidagi yig'indisidan iborat massiv qaytaring. (Object.entries, map, join) Input: {a: 2, b: 5, c: 7}Output: ['a2', 'b5', 'c7']

// function StrArr(obj) {
//     return Object.entries(obj).map(([key, value]) => key + value.toString());
// }
// const obj = {a: 2, b: 5, c: 7};
// const nt = StrArr(obj);
// console.log(nt); 


// 21. Sonning raqamlari yig'indisini hisoblab beradigan digitSum() funksiya yozing. (rekursiv funksiya)

// function digitSum(num) {
//     if (num < 10) {
//       return num;
//     } else {
//       return digitSum(Math.floor(num / 10)) + (num % 10);
//     }
// }
// console.log(digitSum(12345));
  

// 22. Quyidagi pupils massividagi barcha o'quvchilarni protcentlarining o'rtacha qiymatini toping. (reduce)

// let pupils = [
//     { name: "John", score: 80 },
//     { name: "Emma", score: 120 },
//     { name: "David", score: 95 },
//     { name: "Harry", score: 100 },
// ];
// let Score = pupils.reduce((total, pupil) => total + pupil.score, 0) / pupils.length;
// console.log("A score:", Score);
// let newPupils = pupils.map(pupil => {
//     return {
//       ...pupil,
//       percentage: pupil.score / 150 * 100,
//       aboveAverage: pupil.score > Score
//     };
// });  
// console.log(newPupils);


// 23. grade propertyga protcent 90-100 o'rtasida bo'lsa 5, 80-90 o'rtasida bo'lsa 4, 70-80 o'rtasida bo'lsa 3 bahoni, qolgan holatlarda 2 bahoni o'zlashtiring.(map)

// let pupils = [
//     { name: 'Samariddin', grade: 87 },
//     { name: 'Nematullo', grade: 71 },
//     { name: 'Daler', grade: 95 },
//     { name: 'Umurzoq', grade: 68 },
//     { name: 'Muslimbek', grade: 82 }
// ];
// let nPupils = pupils.map(pupil => {
//     let grade = 2;
//     if (pupil.grade >= 90) {
//       grade = 5;
//     } else if (pupil.grade >= 80) {
//       grade = 4;
//     } else if (pupil.grade >= 70) {
//       grade = 3;
//     }
//     return { ...pupil, grade };
// });
// console.log(nPupils);  


// 24. isPassed propertyga protcent 70 dan o'tsa true, aks holda false qiymat o'zlashtirilsin. (map)

// let pupils = [
//     { name: "Samariddin", grade: 85 },
//     { name: "Nematullo", grade: 75 },
//     { name: "Umurzoq", grade: 50 },
//     { name: "Daler", grade: 60 },
// ];
// let Pupils70 = pupils.map((pupil) => {
//     let grade;
//     if (pupil.grade >= 90) {
//       grade = 5;
//     } else if (pupil.grade >= 80 && pupil.grade < 90) {
//       grade = 4;
//     } else if (pupil.grade >= 70 && pupil.grade < 80) {
//       grade = 3;
//     } else {
//       grade = 2;
//     }
//     const isPassed = pupil.grade >= 70;
//     return { ...pupil, grade, isPassed };
// });
// console.log(Pupils70);
  

// 25. Necha kishi imtihondan o'tdi va necha kishi imtihonda o'ta olmadi shuni ham hisoblang. (reduce)

// const pupils = [
//     { name: "Samariddin", protcent: 95,},
//     { name: "Akk", protcent: 75,},
//     { name: "Bir kimdir",  protcent: 80,},
//     { name: "Daler", protcent: 85,},
//     { name: "Ass", protcent: 60,},
//     { name: "Kimdur",protcent: 50,},
//   ];
// function Natj(a, b) {
//     if (b.protcent >= 70) {
//       a.passed += 1;
//     } else {
//       a.failed += 1;
//     }
//     return a;
// }
// const results = pupils.reduce(Natj, {passed: 0, failed: 0});
// console.log(`Imtihondan o'tganlar soni: ${results.passed}, o'ta olmayganlar soni: ${results.failed}`);
  